<?php

/**
 * Return information about this library.
 */


function iptc_get_iptc_metadata($file='') {
  if ($file=='') return false;
  
  $size = getimagesize ( $file, $info);
  $ret = array();
  if(is_array($info)) {
    $iptc = iptcparse($info["APP13"]);
    foreach($iptc as $key => $value) {
      if ($key == '2#025') {
        //keywords
        $tmp_keywords = $value;
      } else if ($key == '2#120') {
        //caption'
        $caption = $value[0];
      }
    }
    //parse $keywords array to omit unwanted tags
    foreach($tmp_keywords as $key => $value) {
      $tmp = $value;
        
      $to_omit = variable_get('iptc_omit_tags', '');
      if (trim($to_omit)!='') {
        $tags = preg_split("/[\s]+/", $to_omit);
        if (in_array($tmp, $tags)) {
          $tmp = '';
        }
      }
      if (trim($tmp)!='') $keywords[] = $tmp;
    }
    
    $ret = array(
      'iptc' => array (
        'caption' => $caption,
        'keywords' => $keywords,
      ),
    );
    return $ret;
  }
  watchdog('iptc', 'lib.php error: No iptc data for '.$file, WATCHDOG_NOTICE);
  return false;
}

function iptc_get_exif_metadata($file='') {
  if ($file=='') return false;
  
  //exif_read_data($filename, 'IFD0' or 'EXIF');
  
  watchdog('iptc', 'lib.php error: No exif data for '.$file, WATCHDOG_NOTICE);
  return false;
}
