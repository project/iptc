<?php

/**
 * This is a part of the iptc module which extends the image module to allow for
 * iptc tag extraction from images
 *
 */


function iptc_get_iptc_metadata($file='') {
  if ($file=='') return false;
  
  $iptc_method = variable_get('iptc_method', -1);
  $iptc_gets = variable_get('iptc_gets', -1);
  $cmd = variable_get('iptc_path_exiv2', '');
  //need to double-check path
  if (!is_executable($cmd)) return;
  
  //parse iptc into usable array
  
  if ($iptc_gets == IPTC_IPTC || $iptc_gets == IPTC_BOTH) {
    //iptc
    $ret = exec($cmd.' -pi '.$file, $ret_iptc);
    //iptc caption
    $caption_key = -1;
    $keywords = array();
    foreach($ret_iptc as $key => $value) {
      if ( stristr($value, 'Iptc.Application2.Caption') ) {
        $caption_key = $key;
      }
      if ( stristr($value, 'Iptc.Application2.Keywords') ) {
        $tmp = preg_replace("@Iptc.Application2.Keywords\s+String\s+\d+\s+@si", '', $value);
        
        //need to strip out unwanted keywords?
        $to_omit = variable_get('iptc_omit_tags', '');
        if (trim($to_omit)!='') {
          $tags = preg_split("/[\s]+/", $to_omit);
          if (in_array($tmp, $tags)) {
            $tmp = '';
          }
        }
        
        if (trim($tmp)!='') $keywords[] = $tmp;
      }
    }
    
    if ($caption_key) {
      $tmp = $ret_iptc[$caption_key];
      $tmp = preg_replace("@Iptc.Application2.Caption\s+String\s+\d+\s+@si", '', $tmp);
      
      $caption = $tmp;
    } else {
      $caption = '';
    }
  }
  
  //format array
  if ($iptc_gets == IPTC_BOTH || $iptc_gets == IPTC_IPTC) {
    $ret = array(
      'iptc' => array(
        'caption' => $caption,
        'keywords' => $keywords,
      ),
    );
  }
  
  //dpm($ret, FALSE, 'ret');
  
  return $ret;
}

function iptc_get_exif_metadata($file='') {
  if ($file=='') return false;
  
  $iptc_method = variable_get('iptc_method', -1);
  $iptc_gets = variable_get('iptc_gets', -1);
  $cmd = variable_get('iptc_path_exiv2', '');
  //need to double-check path
  if (!is_executable($cmd)) return;
  
  //parse exif into usable array
  
  if ($iptc_gets == IPTC_EXIF || $iptc_gets == IPTC_BOTH) {
    //exif
    $ret = exec($cmd.' -Plt '.$file, $ret_exif); // -pt 
    
    $pattern = '/Exif\.((.+)\.(.+))(\s+)(\w+)(\s+)(\d+)(\s+)(.+)/';
    $exif = array();
      
    foreach($ret_exif as $key => $value) {
      preg_match($pattern, $value, $matches);
      $exif_key = $matches[1];
      $exif_part = str_replace('.',' ', $matches[3]);
      $exif_value = $matches[9];
      
      $exif[] = array(
        'full' => $exif_key,
        'key' => $exif_part,
        'value' => $exif_value,
      ); 
    }
    //format array
    if ($iptc_gets == IPTC_BOTH || $iptc_gets == IPTC_EXIF) {
      $ret = array(
        'exif' => array(
          $exif
        ),
      );
    }
  }
  
  return false; //until we have some code here!
}

