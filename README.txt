
iptc is a module that will extract iptc caption and keyword tags from images added to drupal.
It relies on the image module (http://drupal.org/project/image) to function.

It uses a plugin system to allow different libraries to be utilised to extract the data from
an image.

Currently the module supports the standard php functions, the exiftool binary, the exiv2 binary.


This module is sponsored by photoscout.co.uk


Requirements
------------
(optional) http://www.exiv2.org - exiv2 binaries
(optional) http://www.sno.phy.queensu.ca/~phil/exiftool/ - exiftool binary
php5 (not tested on php4)

In the unlikely! event that someone wants to utilise this for their own modules/procedures 
I have added a new function that will correctly load the proper libraries and return the data.
The function is
iptc_get_iptc($filename)
where $filename is a full path to the image.

Todo
----
A future addition may will be the "php jpeg metadata toolkit" and possibly any other applications
that I can find to accomplish this task.

At the present time I'm not taking any notice of the exif tags as this is handled by the
Exif module; but I am in the process of making this module extract the exif tags and store them
in the database rather than reading them on-the-fly.

I've just realised that the exiftool library might not be working, and the php library may be incomplete.

