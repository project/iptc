<?php

/**
 * Return information about this library.
 */

function iptc_get_iptc_metadata($file='') {
  //get exiftool data
  if ($file=='') return false;
  
  $iptc_method = variable_get('iptc_method', -1);
  $iptc_gets = variable_get('iptc_gets', -1);
  $cmd = variable_get('iptc_path_exiftool', '');
  //need to double-check path
  if (!is_executable($cmd)) return;
  
  if ($iptc_gets == IPTC_IPTC || $iptc_gets == IPTC_BOTH) {
    $ret = exec($cmd.' -iptc:all '.$file, $ret_iptc);
    
    //iptc caption
    $caption = '';
    $keywords = array();
    foreach($ret_iptc as $key => $value) {
      list($iptc_key, $iptc_value) = split(':', $value);
      
      //dpm($iptc_key, FALSE, 'iptc_key');
      //dpm($iptc_value, FALSE, 'iptc_value');
      
      if (trim($iptc_key) == 'Caption-Abstract') {
        $caption = $iptc_value;
      }
      if (trim($iptc_key) == 'Keywords') {
        //$tmp = $iptc_value;
        //need to split it into an array
        $tmp = preg_split("/[,]+/", $iptc_value);
        
        //need to strip out unwanted keywords?
        $to_omit = variable_get('iptc_omit_tags', '');
        if (trim($to_omit)!='') {
          $tags = preg_split("/[\s]+/", $to_omit);
          if (in_array($tmp, $tags)) {
            $tmp = '';
          }
        }
        
        if (trim($tmp)!='') $keywords[] = $tmp;
      }
    }
    
  }
  
  if ($iptc_gets == IPTC_EXIF || $iptc_gets == IPTC_BOTH) {
    $ret = exec($cmd.' -exif:all '.$file, $ret_exif);
    
    //exif
    //
    $exif = array();
      
    foreach($ret_exif as $key => $value) {
      list($exif_key, $exif_value) = split(':', $value);
      
      $exif[] = array(
        'full' => '',
        'key' => $exif_key,
        'value' => $exif_value,
      ); 
    }
  }
  
  //format array
  if ($iptc_gets == IPTC_BOTH) {
    $ret = array(
      'iptc' => array(
        'caption' => $caption,
        'keywords' => $keywords,
      ),
      'exif' => array(
        $exif
      ),
    );
  } else if ($iptc_gets == IPTC_IPTC) {
    $ret = array(
      'iptc' => array(
        'caption' => $caption,
        'keywords' => $keywords,
      ),
    );
  } else if ($iptc_gets == IPTC_EXIF) {
    $ret = array(
      'exif' => array(
        $exif
      ),
    );
  }
  
  //dpm($ret, FALSE, 'ret');
  
  return $ret;
}

function iptc_get_exif_metadata($file='') {
  return false; //until we have some code here!
}
